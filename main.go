package main

import (
	"os"
	"log"
	"strings"
	"github.com/joho/godotenv"
	"floatplane/utils"
)

func readEnv(key string) string {
	err := godotenv.Load("test.env")
	if err != nil {
		log.Fatalf("Error loading .env file ERR: %s", err)
	}

	return os.Getenv(key)

}

func main() {
	//fileName := "/var/log/nginx/floatplane.log"
	fileName := "./floatplane.log"

	// open log file
	logFile, err := os.OpenFile(fileName, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()

	// set log out put
	log.SetOutput(logFile)

	// optional: log date-time, filename, and line number
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	// Get Channel List and split
	channelList := strings.Split(readEnv("CHANNEL_ARRAY"), " ")
	for i := 0; i < len(channelList); i++ {
		utils.UpdateOnExpCache(channelList[i])
	}
}
