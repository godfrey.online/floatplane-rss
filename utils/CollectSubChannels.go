package utils

import (
	"log"
	"encoding/json"
	"strings"
	"io/ioutil"
	"floatplane/models"
	"fmt"
)

var slugs []models.Slugs
var rssFeedLinks string


func UpdateOnExpCache(channelName string) {

	// If before of after the "before" var
        filename := ".cache/channel_list_floatplane_"+channelName
	if !FileNewer(filename, -1) {
		subChannels(channelName)
		// Save Cache
		test, _ := json.Marshal(slugs)
		SaveFile(".cache/channel_list_floatplane_" + channelName , string(test))
		log.Printf("CACHE EXPIRED")
	} else {
		log.Printf("USING CACHE")
                readFileCache(channelName)
	}
	
	// Call CreateChannelRSS for each channel and sub channel
	baseURL := "https://www.floatplane.com/api/v3/content/creator?id="
        for _, a := range slugs {
		for _, b := range a.SubSlugs {
			url :=  baseURL + a.ID + "&channel=" + b.ID
			log.Printf(url)
			CreateChannelRSS(url, b.Slug)
		}
        }

	fmt.Println(rssFeedLinks)
	//SaveFile("/usr/share/nginx/html/rss/index.html", rssFeedLinks)
	SaveFile("./index.html", rssFeedLinks)
}

// Read file cache
func readFileCache(channelName string) {
	//Reads file and loads json into struct
        file, _ := ioutil.ReadFile(".cache/channel_list_floatplane_"+channelName)
        _ = json.Unmarshal([]byte(file), &slugs)
}

// Collect floatplane sub channels
func subChannels(perentChannelSlug string) {
	var subSlugs []models.SubSlug
	var creator []models.Creator

	jsonDownloadURL := "https://www.floatplane.com/api/v3/creator/named?creatorURL="

	// Download the json from Floatplane
	url := (jsonDownloadURL + perentChannelSlug)
	body := DownloadAndParse(url)
	
	// Read and pass the JSON
	err := json.Unmarshal(body, &creator)
	if err != nil {
		log.Fatal("Error unmarshalling.", err)
		return
	}

	if len(creator) != 0 { // To ensure that it dosn't over loop for some reason.
		for _, chnl := range creator[0].Channels {
			var channel_name = chnl.Title
		
			// CHANGE ME TO A SIMPLER COMMAND	
			channel_name = strings.ToLower(channel_name)
			channel_name = strings.ReplaceAll(channel_name, " ", "")
			channel_name = strings.ReplaceAll(channel_name, "'", "")
			channel_name = strings.ReplaceAll(channel_name, ",", "")
			channel_name = strings.ReplaceAll(channel_name, ".", "")

			rssFeedLinks = rssFeedLinks + "<a href='" + channel_name + ".xml'>" + channel_name  +  "</a>\n"
			subChannelList := models.SubSlug{
				ID:	chnl.ID,
				Slug:	channel_name,
			}
			subSlugs = append(subSlugs, subChannelList)
		}
		channelList := models.Slugs{
			ID:	creator[0].ID,
			Slug:	perentChannelSlug,
			Platform: "floatplane",
			SubSlugs: subSlugs,
		}

		slugs = append(slugs, channelList)
	}

}
