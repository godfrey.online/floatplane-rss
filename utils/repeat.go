package utils

import (
	"os"
	"log"
	"time"
	"io/ioutil"
	"net/http"
)

// Check if the file is older
func FileNewer(fileLoc string, days int) bool {// CHANGE FUNCTION TO FileOlder
        now := time.Now()
        before := now.AddDate(0, 0, days) // Minus one day


	// If the file doesn't exist, create it, or append to the file
	_, err := os.OpenFile(fileLoc, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

        file, err := os.Stat(fileLoc)
        if err != nil {
                log.Fatal(err)
        }
        modifiedtime := file.ModTime()
    
	// If before of after the "before" var
        if modifiedtime.Before(before) {
		return true 
	} else {
		return false 
	}
}

// Download JSON and return
func DownloadAndParse(url string) []byte {
//func downloadAndParse(url string) string {

	// Download JSON data
	resp, err := http.Get(url)
	if err != nil {
		// log
		log.Fatal("error downloading URL", err)
	}
	defer resp.Body.Close()

	// Parse the HTTP data
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// log
		log.Fatal("Error reading reponse body", err)
	}

	return body
	//return body 
}

// Save file
func SaveFile(location string, data string) {
        
        f, err := os.Create(location)
        if err != nil {
                panic(err)
        }

        // Write Data
        f.WriteString(data)
        if err != nil {
                f.Close()
                return
        }
        err = f.Close()
        if err != nil {
                log.Fatal(err)
                return
        }
}

