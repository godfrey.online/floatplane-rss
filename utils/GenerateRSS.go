package utils

import (
	"log"
	"time"
	"encoding/json"
	"net/http"
	"io/ioutil"
	"github.com/eduncan911/podcast"
	"floatplane/models"
)

func CreateChannelRSS(url string, channel_name string) {

	//        #######################                        FIX THE DUMB HACK!!!  #                       #
	// Some dumb hack to fix the podcast.New bit
	currentTime := time.Now() // CHANGE ME!!!! /// TO FIX YOU COULD USE THE ENV FILE TO LOAD A DATE??? OR JUST SET A RANDOM ONE???

	// Download the json from Floatplane
	// MOVE TO A REUSABLE FUNC
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal("error downloading URL", err)
		return
	}
	defer resp.Body.Close()

	// Read the response body and asign to var body
	// MOVE TO A REUSABLE FUNC
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Error reading reponse body", err)
		return
	}
	
	// Read and pass the JSON
	var post []models.Post
	err = json.Unmarshal(body, &post)
	if err != nil {
		log.Fatal("Error unmarshalling.", err)
		return
	}

	// Initilise the podcast
	p := podcast.New(
		 post[0].Channels.Title + " - Floatplane",
		"https://floatplane.com",
		post[0].Text, 
		&currentTime, &currentTime,
	)

	p.AddSummary(`link <a href="https://floatplane.com">floatplane.com</a>`)
	p.AddImage(post[0].Channels.Icon.Path)

	// loop through the posts var and display the post's data
	for _, po := range post {
	        var imagePath string
		if len(po.Thumbnails.ChildImages) > 0 {
			imagePath = po.Thumbnails.ChildImages[0].Path
       		}
		DscPlusImage := "<img src='" + imagePath +  "'/> " + po.Text 
		// Add RSS item
		item := podcast.Item{
			Title:  	po.Title,
			Link:		"https://www.floatplane.com/post/"+po.ID,
			Description:	DscPlusImage,
			PubDate:	&po.ReleaseDate,
		}

		item.AddImage(imagePath)

		if _, err := p.AddItem(item); err != nil {
			log.Fatal("Error writing to stdout", err.Error())
		}
	}

	// Write RSS Feed
	//fileName := "./" + channel_name + ".xml"
	//fileName := "/usr/share/nginx/html/rss/" + channel_name + ".xml"
	fileName := "tmp/" + channel_name + ".xml"
        SaveFile(fileName, string(p.String()))
}
