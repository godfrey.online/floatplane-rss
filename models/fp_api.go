package models

import "time"

type Creator struct {
	ID		string		`json:"id"`
	Title		string		`json:"title"`
	Channels	[]Channel	`json:"channels"`
}

type Channel struct {
	ID		string		`json:"id"`
	CreatorID	string		`json:"creator"`
	Title		string		`json:"title"`
	URLName		string		`json:"urlname"`
	Icon		Icon		`json:"icon"`
}

type Icon struct {
	ChildImages	[]ChildImages 	`json:childImages`
	Path		string		`json:"path"`
}

type Post struct {
	ID		string 		`json:"id"`
	Title		string		`json:"title"`
	Icon		Icon		`json:"icon"`
	Text		string		`json:"text"`
	ReleaseDate	time.Time	`json:"releaseDate"`
	Thumbnails	Thumbnails	`json:"thumbnail"`
	Channels	Channel		`json:"channel"`
}

type Thumbnails struct {
	Width		int		`json:"width"`
	Height		int		`json:"height"`
	Path		string		`json:"path"`
	ChildImages	[]ChildImages	`json:"childImages"`
}

type ChildImages struct {
	Width		int		`json:"width"`
	Heiht		int		`json:"height"`
	Path		string		`json:"path"`
}

type Slugs struct {
	ID			string			`json:"id"`
	Slug			string			`json:"slug"`
	Platform		string			`json:"platform"`
	SubSlugs		[]SubSlug		`json:"subslug"`
}

type SubSlug struct {
	ID		string		`json:"id"`
	Slug		string		`json:"slug"`
}


