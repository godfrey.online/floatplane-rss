### Floatplane RSS feed genorator

This is a simple golang project created a while ago to create rss feeds for creators over at [Floatplane](https://floatplane.com). You can host this your self or access the hosted version at [godfrey.online/rss](https://godfrey.online/rss/index.html).

If you are looking for [Means.TV](https://means.tv) rss you can find the code also on my git [GIT](https://gitlab.com/godfrey.online/meanstv-rss). 
